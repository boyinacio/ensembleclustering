package br.ufrn.application;

import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.FormatterClosedException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

import jmetal.core.Algorithm;
import jmetal.core.Operator;
import jmetal.core.Problem;
import jmetal.core.Solution;
import jmetal.core.SolutionSet;
import jmetal.core.Variable;
import jmetal.metaheuristics.nsgaII.NSGAII;
import jmetal.operators.crossover.CrossoverFactory;
import jmetal.operators.mutation.MutationFactory;
import jmetal.operators.selection.SelectionFactory;
import jmetal.problems.clustering.EnsembleProblem;
import jmetal.util.Configuration;
import jmetal.util.JMException;
import weka.clusterers.HierarchicalClusterer;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import zelig.util.clusteringEvaluators.BasicDaviesBouldin;
import zelig.util.distances.CoassociationDistance;
import zelig.util.translators.ClusterNumberTranslator;

public class Main {

	public static Logger logger_; // Logger object
	public static FileHandler fileHandler_; // FileHandler object
	public static Instances dataset;
	public static Instances[] particoes, novasParticoes;

	public static void loadDataset(String base) {
		try {
			DataSource source = new DataSource("bases/" + base + ".arff");
			dataset = source.getDataSet();
			dataset.setClassIndex(dataset.numAttributes() - 1);
		} catch (Exception e) {
			System.out.println("Não foi possível carregar a base!!\n\n");
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static Instances translate(Solution solution) throws JMException {
		Variable[] vars = solution.getDecisionVariables(); // Cromossomo
		int[] rotulos = new int[vars.length];

		for (int i = 0; i < rotulos.length; i++) {
			rotulos[i] = ((Double) vars[i].getValue()).intValue();
			rotulos[i] = ((Double) (particoes[rotulos[i]].instance(i)
					.classValue())).intValue();
		}

		return new ClusterNumberTranslator().translate(rotulos, dataset);
	}

	public static void configure(Algorithm algorithm, Problem problem)
			throws JMException {
		Operator crossover; // Crossover operator
		Operator mutation; // Mutation operator
		Operator selection; // Selection operator

		HashMap parameters; // Operator parameters

		// Algorithm parameters
		algorithm.setInputParameter("populationSize", 100);
		algorithm.setInputParameter("maxEvaluations", 25000);

		// Mutation and Crossover for Real codification
		parameters = new HashMap();
		parameters.put("probability", 0.9);
		parameters.put("distributionIndex", 20.0);
		crossover = CrossoverFactory.getCrossoverOperator(
				"SinglePointCrossover", parameters);

		parameters = new HashMap();
		parameters.put("probability", 1.0 / problem.getNumberOfVariables());
		parameters.put("distributionIndex", 20.0);
		mutation = MutationFactory.getMutationOperator("BitFlipMutation",
				parameters);

		// Selection Operator
		parameters = null;
		selection = SelectionFactory.getSelectionOperator("BinaryTournament2",
				parameters);

		// Add the operators to the algorithm
		algorithm.addOperator("crossover", crossover);
		algorithm.addOperator("mutation", mutation);
		algorithm.addOperator("selection", selection);

		// Add the indicator object to the algorithm
		algorithm.setInputParameter("indicators", null);
	}

	public static void saveResults(double dbIndex) {
		Formatter saver = null;

		try {
			saver = new Formatter("FUN_ENSEMBLE");
		} catch (SecurityException securityException) {
			System.err.println("You do not have write access to this file.");
			System.exit(1);
		} catch (FileNotFoundException e) {
			System.err.println("Error opening or creating file.");
			System.exit(1);
		}

		try {
			saver.format("%f\n", dbIndex);
		} catch (FormatterClosedException formatterClosedException) {
			System.err.println("Error writing to file.");
			return;
		} catch (NoSuchElementException elementException) {
			System.err.println("Invalid input. Please try again.");
			return;
		}

		saver.close();

	}

	public static void main(String[] args) throws Exception {
		logger_ = Configuration.logger_;
		fileHandler_ = new FileHandler("Ensemble_Clusering_plus_NSGAII.log");
		logger_.addHandler(fileHandler_);

		// Instacia e configura o algoritmo e o problema
		// -------------------------------------------------------------
		EnsembleProblem problem; // The problem to solve
		Algorithm algorithm; // The algorithm to use

		loadDataset("iris");

		problem = new EnsembleProblem(dataset, dataset.numClasses());
		particoes = problem.getParticoes();

		algorithm = new NSGAII(problem);
		configure(algorithm, problem);
		// -------------------------------------------------------------

		// Executa o algoritmo
		// -------------------------------------------------------------
		long initTime = System.currentTimeMillis();
		SolutionSet population = algorithm.execute();
		long estimatedTime = System.currentTimeMillis() - initTime;
		// -------------------------------------------------------------

		// Aplica a coassiociação nas soluções
		// -------------------------------------------------------------
		novasParticoes = new Instances[population.size()];
		Instances datasetCopy = new Instances(dataset);
		datasetCopy.setClassIndex(-1);

		Remove filter = new Remove();
		try {
			filter.setAttributeIndices("" + (dataset.classIndex() + 1));
			filter.setInputFormat(dataset);
			datasetCopy = Filter.useFilter(dataset, filter);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}

		for (int i = 0; i < novasParticoes.length; i++) {
			novasParticoes[i] = translate(population.get(i));
		}

		HashMap<Instance, Integer> instanceIndexMap = new HashMap<Instance, Integer>();

		for (int i = 0; i < datasetCopy.numInstances(); i++) {
			instanceIndexMap.put(datasetCopy.instance(i), i);
		}

		CoassociationDistance distance = new CoassociationDistance(
				novasParticoes, datasetCopy, instanceIndexMap, false);
		
		// Clusteriza a base com base no hierarquico
		// -------------------------------------------------------------		
		HierarchicalClusterer clusterer = new HierarchicalClusterer();

		clusterer.setDistanceFunction(distance);
		clusterer.setNumClusters(dataset.numClasses());
		clusterer.buildClusterer(datasetCopy);

		ClusterNumberTranslator translator = new ClusterNumberTranslator();
		int[] labels = new int[datasetCopy.numInstances()];
		int sz = clusterer.numberOfClusters();

		for (int i = 0; i < datasetCopy.numInstances(); i++) {
			try {
				labels[i] = clusterer.clusterInstance(datasetCopy.instance(i));
			} catch (Exception e) {
				labels[i] = sz + 1;
			}
		}

		datasetCopy = translator.translate(labels, datasetCopy);
		// -------------------------------------------------------------

		// -------------------------------------------------------------

		// Result messages
		// -------------------------------------------------------------
		double db = BasicDaviesBouldin.main(datasetCopy); 
		System.out.println("DB da coassociação: " + db);

		logger_.info("Total execution time: " + estimatedTime + "ms");
		logger_.info("Objectives values have been writen to file FUN");
		population.printObjectivesToFile("FUN");
		logger_.info("Ensemble DB Index value have been writen to file FUN_ENSEMBLE");
		saveResults(db);
		// -------------------------------------------------------------

	} // main
} // NSGAII_main
